#Currency converter

This service performs simple convertation of price by predefined currencies values.

## How to build project

To build jar artifact please use Maven command

```mvn clean install```

## How to start

Build docker image
```
docker build -t currency-converter:0.1 .
 ```
 
 Run service on 8080 port
 
```
docker run -p 8080:8080 currency-converter:0.1
 ```

## Request format

Here's example of curl request with required format of request

```
curl -i -X POST localhost:8080/api/converter -H "Content-Type: application/json" -d '{"from_currency":"USD", "to_currency":"PLN", "value":1}'
```

## Run generation of mutation coverage

```mvn org.pitest:pitest-maven:mutationCoverage```

This will output an html report to target/pit-reports/YYYYMMDDHHMI