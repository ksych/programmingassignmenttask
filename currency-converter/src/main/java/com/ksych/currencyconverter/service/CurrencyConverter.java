package com.ksych.currencyconverter.service;

import com.ksych.currencyconverter.api.model.ConverterRequest;
import com.ksych.currencyconverter.api.model.ConverterResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
public class CurrencyConverter {

    private CurrencyProvider currencyProvider;

    public CurrencyConverter(@Autowired CurrencyProvider currencyProvider) {
        this.currencyProvider = currencyProvider;
    }

    public ConverterResponse convert(ConverterRequest request) {
        BigDecimal currencyValue = currencyProvider.getCurrencyValue(request.getFromCurrency(), request.getToCurrency());
        return new ConverterResponse(currencyValue.multiply(request.getValue()));
    }
}
