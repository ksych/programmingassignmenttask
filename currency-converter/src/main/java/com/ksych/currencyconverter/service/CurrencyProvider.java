package com.ksych.currencyconverter.service;

import com.ksych.currencyconverter.api.model.NotFoundCurrency;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
@ConfigurationProperties(prefix = "currency")
public class CurrencyProvider {

    @Getter
    private final Map<String, Map<String, BigDecimal>> currencyMap = new HashMap<>();

    public BigDecimal getCurrencyValue(String from, String to) {
        Map<String, BigDecimal> fromCurrency = currencyMap.get(from);
        if (Objects.nonNull(fromCurrency)) {
            if (fromCurrency.containsKey(to)) {
                return fromCurrency.get(to);
            }
            throw new NotFoundCurrency("No such TO currency");
        } else {
            throw new NotFoundCurrency("No such FROM currency");
        }
    }
}
