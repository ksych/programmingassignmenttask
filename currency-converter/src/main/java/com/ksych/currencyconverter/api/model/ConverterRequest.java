package com.ksych.currencyconverter.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConverterRequest {

    @NotBlank
    private String fromCurrency;
    @NotBlank
    private String toCurrency;
    @PositiveOrZero
    private BigDecimal value;

}
