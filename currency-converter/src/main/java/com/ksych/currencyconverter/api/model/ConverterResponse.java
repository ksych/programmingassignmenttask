package com.ksych.currencyconverter.api.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ConverterResponse {

    private final BigDecimal converted;
}
