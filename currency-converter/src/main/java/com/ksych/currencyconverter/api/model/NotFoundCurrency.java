package com.ksych.currencyconverter.api.model;

public class NotFoundCurrency extends RuntimeException {

    public NotFoundCurrency(String mes) {
        super(mes);
    }
}
