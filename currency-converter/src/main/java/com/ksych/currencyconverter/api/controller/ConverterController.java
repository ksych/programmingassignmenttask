package com.ksych.currencyconverter.api.controller;

import com.ksych.currencyconverter.api.model.ConverterRequest;
import com.ksych.currencyconverter.api.model.ConverterResponse;
import com.ksych.currencyconverter.service.CurrencyConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController("api/converter")
@Slf4j
public class ConverterController {

    @Autowired
    private CurrencyConverter currencyConverter;

    @PostMapping
    @ResponseBody
    public ConverterResponse convertAmount(@RequestBody @Valid ConverterRequest request) {
        log.debug("Request to convert - {}", request);
        return currencyConverter.convert(request);
    }
}
