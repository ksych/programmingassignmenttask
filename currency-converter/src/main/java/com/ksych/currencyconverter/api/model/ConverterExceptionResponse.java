package com.ksych.currencyconverter.api.model;

import lombok.Data;

@Data
public class ConverterExceptionResponse {
    private final String message;
}
