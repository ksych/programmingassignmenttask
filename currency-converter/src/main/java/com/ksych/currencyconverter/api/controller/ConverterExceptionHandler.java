package com.ksych.currencyconverter.api.controller;

import com.ksych.currencyconverter.api.model.ConverterExceptionResponse;
import com.ksych.currencyconverter.api.model.NotFoundCurrency;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ConverterExceptionHandler {

    @ExceptionHandler
    public ResponseEntity handleInvalidRequest(MethodArgumentNotValidException ex) {
        log.debug("Invalid request ", ex);
        return buildEntity(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity handleNotFoundCurrency(NotFoundCurrency ex) {
        log.info("User requested currency that was not set by properties", ex);
        return buildEntity(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity handleUnexpectedException(RuntimeException ex) {
        log.error("Unexpected error", ex);
        return buildEntity(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity buildEntity(Exception ex, HttpStatus status) {
        return new ResponseEntity<>(new ConverterExceptionResponse(ex.getMessage()), status);
    }

}
