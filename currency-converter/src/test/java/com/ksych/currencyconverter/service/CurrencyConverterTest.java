package com.ksych.currencyconverter.service;

import com.ksych.currencyconverter.api.model.ConverterRequest;
import com.ksych.currencyconverter.api.model.ConverterResponse;
import com.ksych.currencyconverter.api.model.NotFoundCurrency;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyConverterTest {

    @Mock
    private CurrencyProvider currencyProvider;

    private CurrencyConverter testee;

    private final String FROM = "FROM";
    private final String TO = "TO";

    @Before
    public void init() {
        testee = new CurrencyConverter(currencyProvider);
    }

    @Test
    public void convert_success() {
        ConverterRequest request = new ConverterRequest(FROM, TO, new BigDecimal(25));
        when(currencyProvider.getCurrencyValue(FROM, TO)).thenReturn(new BigDecimal(0.56));

        ConverterResponse convert = testee.convert(request);

        assertEquals(14, convert.getConverted().doubleValue(), 0.5);
    }


    @Test(expected = NotFoundCurrency.class)
    public void convert_providerFail() {
        ConverterRequest request = new ConverterRequest(FROM, TO, new BigDecimal(25));
        when(currencyProvider.getCurrencyValue(FROM, TO)).thenThrow(NotFoundCurrency.class);

        testee.convert(request);
    }
}
