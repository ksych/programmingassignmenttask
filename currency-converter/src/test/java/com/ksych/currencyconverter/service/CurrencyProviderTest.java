package com.ksych.currencyconverter.service;

import com.google.common.collect.ImmutableMap;
import com.ksych.currencyconverter.api.model.NotFoundCurrency;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyProviderTest {

    private CurrencyProvider testee = new CurrencyProvider();

    @Before
    public void init() {
        testee.getCurrencyMap().putIfAbsent("USD", ImmutableMap.of("PLN", new BigDecimal(0.5)));
    }

    @Test
    public void getCurrencyValue_success() {
        BigDecimal currencyValue = testee.getCurrencyValue("USD", "PLN");

        assertEquals(0.5, currencyValue.doubleValue(), 0);
    }

    @Test(expected = NotFoundCurrency.class)
    public void getCurrencyValue_fromCurrencyAbsent() {
        BigDecimal currencyValue = testee.getCurrencyValue("BGN", "PLN");
    }

    @Test(expected = NotFoundCurrency.class)
    public void getCurrencyValue_toCurrencyAbsent() {
        BigDecimal currencyValue = testee.getCurrencyValue("USD", "BGN");
    }
}
