package com.ksych.currencyconverter.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ksych.currencyconverter.api.model.ConverterRequest;
import com.ksych.currencyconverter.api.model.ConverterResponse;
import com.ksych.currencyconverter.api.model.NotFoundCurrency;
import com.ksych.currencyconverter.service.CurrencyConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ConverterController.class)
@AutoConfigureMockMvc
public class ConverterControllerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CurrencyConverter currencyConverter;

    @Test
    public void convertAmount_success() throws Exception {
        when(currencyConverter.convert(any(ConverterRequest.class))).thenReturn(new ConverterResponse(BigDecimal.valueOf(42)));
        this.mockMvc.perform(
                post("/api/converter")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                objectMapper.writeValueAsString(
                                        new ConverterRequest("from", "to", BigDecimal.ONE))))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("42")));
    }

    @Test
    public void convertAmount_notFoundCurrencyShouldReturnInvalid() throws Exception {
        String errorMessage = "Error message";
        when(currencyConverter.convert(any(ConverterRequest.class))).thenThrow(new NotFoundCurrency(errorMessage));
        this.mockMvc.perform(
                post("/api/converter")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                objectMapper.writeValueAsString(
                                        new ConverterRequest("from", "to", BigDecimal.ONE))))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString(errorMessage)));
    }

    @Test
    public void convertAmount_failValidationFrom() throws Exception {
        when(currencyConverter.convert(any(ConverterRequest.class))).thenReturn(new ConverterResponse(BigDecimal.valueOf(42)));
        this.mockMvc.perform(
                post("/api/converter")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                objectMapper.writeValueAsString(
                                        new ConverterRequest(null, "to", BigDecimal.ONE))))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void convertAmount_failValidationTo() throws Exception {
        when(currencyConverter.convert(any(ConverterRequest.class))).thenReturn(new ConverterResponse(BigDecimal.valueOf(42)));
        this.mockMvc.perform(
                post("/api/converter")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                objectMapper.writeValueAsString(
                                        new ConverterRequest("from", null, BigDecimal.ONE))))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void convertAmount_failValidationValud() throws Exception {
        when(currencyConverter.convert(any(ConverterRequest.class))).thenReturn(new ConverterResponse(BigDecimal.valueOf(42)));
        this.mockMvc.perform(
                post("/api/converter")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                objectMapper.writeValueAsString(
                                        new ConverterRequest("from", "to", BigDecimal.valueOf(-42)))))
                .andExpect(status().is4xxClientError());
    }

}
